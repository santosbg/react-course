import React from "react";
import ReactDOM from "react-dom";
import SeasonDisplay from "./SeasonDisplay";
import Spinner from "./Spinner";

class App extends React.Component {
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     latitude: null,
  //     errorMessage: ''
  //   };
  // }

  state = {
    latitude: null,
    errorMessage: ''
  };

  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      position => this.setState({ latitude: position.coords.latitude }),
      error => this.setState({ errorMessage: error.message })
    );
  }

  componentDidUpdate() {
    console.log(new Date().toISOString())
  }

  render() {
    if(this.state.errorMessage && !this.state.latitude) {
      return <div>Error: { this.state.errorMessage }</div>;
    }

    if(!this.state.errorMessage && this.state.latitude) {
      return <SeasonDisplay latitude={this.state.latitude}/>;
    }

    return  <Spinner text="Please accept location request" />
  }
};

ReactDOM.render(<App />, document.getElementById('root'));