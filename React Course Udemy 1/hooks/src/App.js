import React, { useState } from 'react';

import Accordion from './components/Accordion';
import Search from './components/Search';
import DropDown from './components/DropDown';
import Translate from './components/Translate';

const items = [
  {
    title: 'What is React',
    content: 'React is cool shit'
  },
  {
    title: 'What is Angular',
    content: 'Angular is old shit'
  },
  {
    title: 'What is Vue',
    content: 'Vue is new shit'
  },
];

const options = [
  {
    label: 'Red',
    value: 'red'
  },
  {
    label: 'Blue',
    value: 'blue'
  },
  {
    label: 'Green',
    value: 'green'
  },
];

export default () => {
  const [selected, setSelected] = useState(options[0]);

  return (
    <div>
      {/* <Accordion items={items} /> */}
      {/* <Search /> */}
      {/* <DropDown
        options={options}
        selected={selected}
        onSelectedChange={setSelected}
        label="color"
      />  */}
      <Translate />
    </div>
  );
};