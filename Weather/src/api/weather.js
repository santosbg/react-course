import axios from 'axios';

const weatherApi = axios.create({ baseURL: 'http://api.openweathermap.org' });
const weatherKey = '4a65b52f4383ee71d8125f53feab0609';
export {
  weatherApi,
  weatherKey
}