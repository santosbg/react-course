import React, { useState, useEffect, useRef } from 'react';

const DropDown = ({ options, selected, onSelectedChange, label }) => {
  const [open, setOpen] = useState(false);
  const ref = useRef();

  useEffect(() => {
    const onBodyClicked = e => {
      if(ref.current.contains(e.target)) {
        return;
      }
      setOpen(false);      
    };

    document.body.addEventListener('click', onBodyClicked);

    return () => {
      document.body.removeEventListener('click', onBodyClicked);
    };
  }, []);

  const renderOptions = options.map(option => {
    if(option.value === selected.value) {
      return null;
    }

    return (
      <div
        className="item"
        key={option.value}
        onClick={() => onSelectedChange(option)}
      >
        {option.label}
      </div>
    );
  });
  
  return (
    <div ref={ref} className="ui form">
      <div className="field">
        <label className="label">Select a {label}</label>
        <div
          className={`ui selection dropdown ${open ? 'visible active': ''}`}
          onClick={() => setOpen(!open)}
        >
          <i className="dropdown icon"></i>
          <div className="text">{selected.label}</div>
          <div className={`menu ${ open ? 'visible transition' : 0}`}>{renderOptions}</div>
        </div>
      </div>
    </div>
  );
};

export default DropDown;