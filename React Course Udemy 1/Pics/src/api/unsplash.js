import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID 3QPH3qEMfuP3j2nV6etALN094PyXokdir5oAHCbc3c0'
  }
});