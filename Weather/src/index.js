import React from 'react';
import ReactDOM from 'react-dom';

import Spinner from './Spinner';

import { weatherApi, weatherKey } from './api/weather';

class App extends React.Component {
  state = {
    weather: 'loading',
    video: 'cloudy.mp4',
    loading: true
  };

  
  async componentDidMount() {
    const response = await weatherApi.get(`data/2.5/weather?q=Sofia&appid=${weatherKey}`);
    const weather = response.data.weather[0].description;
    console.log(response)
    this.setState({
      weather,
      loading: false
    });
  }

  render() {
    if(this.state.loading) {
      return <Spinner text="Just a second..." />;
    }

    return (
      <video loop autoPlay muted controls id="1">
        <source type="video/mp4" src={this.state.video} />
      </video>
    );
  }
};

ReactDOM.render(<App />, document.getElementById('root'));