import { combineReducers } from 'redux';

const songsReducer = () => {
  return [
    {
      title: '300',
      author: 'MJ',
      duration: '4:05'
    },
    {
      title: 'Will',
      author: 'Will Smith',
      duration: '3:15'
    },
    {
      title: 'Loose my self',
      author: 'Eminem',
      duration: '3:50'
    },
    {
      title: 'Hustler',
      author: '50 cent',
      duration: '3:40'
    }
  ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if(action.type === 'SONG_SELECTED') {
    return action.payload;
  }

  return selectedSong;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer
});