import React from "react";
import ReactDOM from "react-dom";
import CommentDetail from "./CommentDetail";
import faker from "faker";
import ApprovalCard from "./ApprovalCard";

const App = () => {
  return (
    <div className="ui container comments">
      <ApprovalCard>
        <CommentDetail
          author="Sam"
          time="18:00"
          avatar={faker.image.avatar()}
          text="Great stuff"
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author="Ben"
          time="19:17"
          avatar={faker.image.avatar()}
          text="Good job!"
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author="Tod"
          time="20:22"
          avatar={faker.image.avatar()}
          text="Awesome."
        />
      </ApprovalCard>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
