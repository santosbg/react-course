import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import StreamCreate from './streams/StreamCreate';
import StreamShow from './streams/StreamShow';
import StreamEdit from './streams/StreamEdit';
import StreamDelete from './streams/StreamDelete';
import StreamList from './streams/StreamList';
import Header from './Header';

const App = () => {
  return (
    <div className="ui container">
      <BrowserRouter>
        <div>
          <Header />
          <Route path="/" exact component={StreamList}></Route>
          <Route path="/stream/show" component={StreamShow}></Route>
          <Route path="/stream/create" component={StreamCreate}></Route>
          <Route path="/stream/edit" component={StreamEdit}></Route>
          <Route path="/stream/delete" component={StreamDelete}></Route>
        </div>
      </BrowserRouter>
    </div>
  );
};

export default App;